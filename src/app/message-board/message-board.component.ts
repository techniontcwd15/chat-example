import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.css'],
  providers: [RestService]
})
export class MessageBoardComponent implements OnInit {
  private messages = [];

  constructor(private restService:RestService) {}

  ngOnInit() {
    this.restService.messages.subscribe((newMessages:any) => {
      this.messages = newMessages;
    });
  }

}
