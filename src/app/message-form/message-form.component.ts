import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css'],
  providers: [RestService]
})
export class MessageFormComponent implements OnInit {
  messageStatus = '';
  messageField = '';
  userField = '';

  constructor(private restService:RestService) {}

  ngOnInit() {
  }

  sendMessage(username, message) {
    this.messageField = '';
    this.userField = '';
    
    this.restService.sendMessage(username, message)
      .subscribe(() => {
        this.messageStatus = 'OK!';
      })
  }

}
