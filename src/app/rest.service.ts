import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class RestService {
  public messages = new Subject();

  constructor(private http: Http) {
    this.getMessages();
  }

  private getMessages() {
    this.http.get('https://hidden-headland-7200.herokuapp.com/')
      .map(res => res.json())
      .subscribe((newMessages) => {
        this.messages.next(newMessages);
        setTimeout(() => this.getMessages(), 500);
      });
  }

  sendMessage(name, message) {
    return this.http.post('https://hidden-headland-7200.herokuapp.com/new',
    {name, message});
  }

}
